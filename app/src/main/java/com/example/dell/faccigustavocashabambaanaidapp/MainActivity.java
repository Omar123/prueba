package com.example.dell.faccigustavocashabambaanaidapp;




import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private EditText cent, far;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("MainActivity", "GUSTAVO OMAR CASHABAMABA QUILLIGANA");

        this.cent = (EditText)findViewById(R.id.Centigrados);
        this.far = (EditText)findViewById(R.id.Farenheit) ;

        //evento
        this.cent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //calculo
                float farent;
                farent = (1.8f)*Float.parseFloat(cent.getText().toString())+32;
                far.setText(""+farent);

                return false;
            }

        });

        this.far.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //calculo
                float centi;
                centi = (Float.parseFloat(far.getText().toString())-32)/(1.8f);
                cent.setText(""+centi);
                return false;
            }
        });
    }
}

